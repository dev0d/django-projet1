from django.db import models

# Create your models here.
from django.contrib.auth.models import AbstractUser

class Utilisateur(AbstractUser):

	a_vote = models.BooleanField(default=False)
	date_vote = models.DateTimeField(null=True, blank=True)


class Candidat(models.Model):

	lastname = models.CharField(max_length=30)
	firstname = models.CharField(max_length=30)
	section = models.CharField(max_length=30)
	description = models.TextField()
	image = models.ImageField(upload_to = 'pic_folder/', default= 'pic_folder/no_img.jpg')	

class CandidatsNbVote(models.Model):

	nb_votes = models.IntegerField(default=0)
	ref_candidat = models.ForeignKey(Candidat, on_delete=models.CASCADE)

class HTMLTemplate(models.Model):

	key = models.CharField(primary_key=True, max_length=30)
	value = models.TextField(max_length=1000)
	description = models.CharField(max_length=300)

	def __str__(self):
		return self.key;

class Config(models.Model):
	indiquer_si_les_votes_sont_actifs = models.BooleanField(default=False)
	date_debut = models.DateTimeField()
	date_fin = models.DateTimeField()
