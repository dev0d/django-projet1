from django.contrib import admin
from .models import *
from django.contrib.auth.admin import UserAdmin
# Register your models here.
class HTMLTemplateAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'description')

admin.site.register(Utilisateur, UserAdmin)
admin.site.register(Candidat)
admin.site.register(HTMLTemplate, HTMLTemplateAdmin)
admin.site.register(CandidatsNbVote)
admin.site.register(Config)