from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views import generic
from django.contrib.auth.views import LoginView
from django.views import View

from .models import *

# Create your views here.

class IndexView(generic.ListView):
	template_name = 'index.html'

	model = Candidat


class login(View):

	title = HTMLTemplate.objects.get(key='main_title')

	def post(self, request):
		form = NameForm(request.POST)

		if form.is_valid():
			user = authenticate(username=request.POST.get('id', ''), password=request.POST.get('passwd', ''))

			if user is not None:
				login(request, user)
				return redirect('/vote')

		return render(request, '../login.html', locals())

	def get(self, request):
		form = NameForm()
		return render(request, '../login.html', locals())

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)

		context['title'] = HTMLTemplate.objects.get(key='main_title')

		return context
