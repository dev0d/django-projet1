from django.urls import path
from django.contrib.auth import views as auth_views

from . import views

app_name = 'vote'

urlpatterns = [
	path('', views.IndexView.as_view(), name="index"),
	path('login/', views.LoginView.as_view(), name='login'),
]